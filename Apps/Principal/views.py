from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView

# Create your views here.
from Apps.Principal.models import *

def info():
    informacion = InformacionGeneral.objects.get(id='1')
    return informacion


class Inicio(TemplateView):
    template_name ="Principal/InicioCV.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Inicio, self).get_context_data(*args, **kwargs)
        context['persona'] = info()

        hab = Habilidades.objects.filter()
        context['habilidades'] = hab

        return context

class Educacion(TemplateView):
    template_name = "Principal/EducacionCV.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Educacion, self).get_context_data(*args, **kwargs)
        context['persona'] = info()

        edu = Formaciones.objects.filter().order_by('-anio_egreso')
        context['educacion'] = edu

        return context

class Experiencia(TemplateView):
    template_name = "Principal/ExperienciaCV.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Experiencia, self).get_context_data(*args, **kwargs)
        context['persona'] = info()

        exp = Experiencias.objects.filter().order_by('-id')
        context['experiencia'] = exp

        return context

class Habilidad(TemplateView):
    template_name = "Principal/HabilidadCV.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Habilidad, self).get_context_data(*args, **kwargs)
        context['persona'] = info()

        hab = []

        tipos = Tipo_Conocimiento.objects.filter().order_by('-id')

        for tipo in tipos:
            tp = Conocimientos.objects.filter(tipo=tipo.id)

            q = {'titulo': tipo.nombre, 'conocimientos': tp}
            hab.append(q)

        print(hab)
        context['habilidades'] = hab

        return context

class Trabajo(TemplateView):
    template_name = "Principal/TrabajosCV.html"

    def get_context_data(self, *args, **kwargs):
        context = super(Trabajo, self).get_context_data(*args, **kwargs)
        context['persona'] = info()

        tipos = [{'tipo':'Web'},{'tipo':'Móvil'}]

        print(tipos)

        trab = Trabajos.objects.filter()

        print(trab)
        context['tipos'] = tipos
        context['trabajos'] = trab

        return context

class Contacto(TemplateView):
    template_name = "Principal/ContactoCV.html"