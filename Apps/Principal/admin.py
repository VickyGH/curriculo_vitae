from django.contrib import admin

# Register your models here.
from Apps.Principal.models import *


class Info_Admin(admin.ModelAdmin):
    fields = ['nombre','paterno','materno','telefono','correo','direccion','fecha_nac','fotografia']
    list_display = fields
    search_fields = ['nombre','paterno','materno','telefono','correo','direccion','fecha_nac','fotografia']

class Hab_Admin(admin.ModelAdmin):
    fields = ['habilidad','porcentaje','descripcion']
    list_display = fields
    search_fields = ['habilidad','porcentaje','descripcion']


class Form_Admin(admin.ModelAdmin):
    fields = ['escuela','titulo','anio_egreso','ubicacion','logo']
    list_display = fields
    search_fields = ['escuela','titulo','anio_egreso','ubicacion','logo']


class Tipo_Con_Admin(admin.ModelAdmin):
    fields = ['nombre']
    list_display = fields
    search_fields = ['nombre']

class Act_Admin(admin.ModelAdmin):
    fields = ['actividad']
    list_display = fields
    search_fields = ['actividad']

class Logros_Admin(admin.ModelAdmin):
    fields = ['logro']
    list_display = fields
    search_fields = ['logro']

class Conoc_Admin(admin.ModelAdmin):
    fields = ['conocimiento','porcentaje','tipo']
    list_display = fields
    search_fields = ['conocimiento','porcentaje','tipo']

class Exp_Admin(admin.ModelAdmin):
    fields = ['empresa','fecha_ini','fecha_fin','puesto','actividad','logros','conocimiento']
    #list_display = fields
    #search_fields = ['empresa','periodo','puesto','actividad','logros','conocimiento']

class Trab_Admin(admin.ModelAdmin):
    fields = ['nombre','url','imagen','lugar','tipo']
    list_display = fields
    search_fields = ['nombre','url','imagen','lugar','tipo']

admin.site.register(InformacionGeneral, Info_Admin)
admin.site.register(Habilidades, Hab_Admin)
admin.site.register(Formaciones, Form_Admin)

admin.site.register(Actividades,Act_Admin)
admin.site.register(Logros,Logros_Admin)

admin.site.register(Tipo_Conocimiento,Tipo_Con_Admin)
admin.site.register(Conocimientos,Conoc_Admin)
admin.site.register(Experiencias,Exp_Admin)
admin.site.register(Trabajos,Trab_Admin)
