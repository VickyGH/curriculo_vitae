import os

import datetime
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

# Create your models here.
Years = (
    (2010, 2010),
    (2011, 2011),
    (2012, 2012),
    (2013, 2013),
    (2014, 2014),
    (2015, 2015),
    (2016, 2016),
    (2017, 2017),
    (2018, 2018)
)

TipoMW = (
    ('Móvil','Móvil'),
    ('Web','Web'),
)

def upload_foto(instance, filename):
    return os.path.join('Foto', 'VGH', filename)

def upload_logos(instance, filename):
    return os.path.join('Foto', 'Logos', filename)

def upload_img(instance, filename):
    return  os.path.join('Foto','Trabajos', filename)


# Create your models here.
class InformacionGeneral(models.Model):
    nombre = models.CharField(max_length=100, unique=True)
    paterno = models.CharField(max_length=100, unique=True)
    materno = models.CharField(max_length=100, unique=True)
    telefono = models.CharField(max_length=12,unique=True)
    correo = models.CharField(max_length=100, unique=True)
    direccion = models.CharField(max_length=150)
    fecha_nac = models.DateField()
    fotografia = models.FileField(upload_to=upload_foto)

    def __str__(self):
        return '%s' % (self.nombre)

    class Meta:
        verbose_name_plural = "Información General"

class Habilidades(models.Model):
    habilidad = models.CharField(max_length=150, unique=True)
    porcentaje = models.FloatField(default=0)
    descripcion = models.CharField(max_length=150)

    def __str__(self):
        return '%s' % (self.habilidad)

    class Meta:
        verbose_name_plural = "Habilidades"

class Formaciones(models.Model):
    escuela=models.CharField(max_length=200)
    titulo=models.CharField(max_length=250)
    anio_egreso = models.IntegerField(choices=Years)
    ubicacion = models.CharField(max_length=250)
    logo = models.ImageField(upload_to=upload_logos)

    def __str__(self):
        return '%s' % (self.titulo)

    class Meta:
        verbose_name_plural = "Formación Escolar"


class Tipo_Conocimiento(models.Model):
    nombre = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return '%s' % (self.nombre)

    class Meta:
        verbose_name_plural = "Tipo de Conocimiento"

class Actividades(models.Model):
    actividad = models.CharField(max_length=300, unique=True)

    def __str__(self):
        return '%s' % (self.actividad)

    class Meta:
        verbose_name_plural = "Actividades"

class Logros(models.Model):
    logro = models.CharField(max_length=400, unique=True)

    def __str__(self):
        return '%s' % (self.logro)

    class Meta:
        verbose_name_plural = "Logros"


class Conocimientos(models.Model):
    conocimiento = models.CharField(max_length=150, unique=True)
    porcentaje = models.FloatField(default=0)
    tipo = models.ForeignKey(Tipo_Conocimiento, on_delete=False)

    def __str__(self):
        return '%s' % (self.conocimiento)

    class Meta:
        verbose_name_plural = "Conocimientos"


class Experiencias(models.Model):
    empresa = models.CharField(max_length=200)
    fecha_ini = models.CharField(max_length=10)
    fecha_fin = models.CharField(max_length=10)
    puesto = models.CharField(max_length=200)
    actividad = models.ManyToManyField(Actividades)
    logros = models.ManyToManyField(Logros)
    conocimiento = models.ManyToManyField(Conocimientos)

    def __str__(self):
        return '%s' % (self.empresa)

    class Meta:
        verbose_name_plural = "Experiencias"


class Trabajos(models.Model):
    nombre = models.CharField(max_length=250)
    url = models.CharField(max_length=250)
    imagen = models.ImageField(upload_to=upload_img)
    lugar = models.ForeignKey(Experiencias, on_delete=False)
    tipo = models.CharField(max_length=50, choices=TipoMW)

    def __str__(self):
        return '%s' % (self.nombre)

    class Meta:
        verbose_name_plural = "Trabajos"