from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required


from .views import *

app_name = 'Principal'
urlpatterns = [
    url(r'^$', Inicio.as_view(), name='Inicio'),
    url(r'^Educacion/$', Educacion.as_view(), name='Educacion'),
    url(r'^Experiencia/$', Experiencia.as_view(), name='Experiencia'),
    url(r'^Habilidad/$', Habilidad.as_view(), name='Habilidad'),
    url(r'^Trabajos/$', Trabajo.as_view(), name='Trabajos'),
    url(r'^Contacto/$', Contacto.as_view(), name='Contacto'),
]

