# Generated by Django 2.1.7 on 2019-03-09 02:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Principal', '0005_trabajos'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='experiencias',
            name='periodo',
        ),
        migrations.AddField(
            model_name='experiencias',
            name='fecha_fin',
            field=models.CharField(default=1, max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='experiencias',
            name='fecha_ini',
            field=models.CharField(default=1, max_length=10),
            preserve_default=False,
        ),
    ]
